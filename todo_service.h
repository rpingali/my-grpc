#include <iostream>
#include <string>
#include <unordered_map>
#include <memory>
#include <unordered_set>

#include "api/todo/v1/todo.grpc.pb.h"


enum Topics
{
    CREATED = 0,
    COMPLETED = 1,
}; 

const int kMaxTopics = 2;

class LocalTodo 
{
    public:
        LocalTodo(std::string Id, bool completed) :mId(Id),mCompleted(completed) {}
        std::string mId;
        bool        mCompleted;
};

class TodoContainer
{
    public:

    TodoContainer(){}
    void createTodo(LocalTodo todo);
    std::shared_ptr<LocalTodo> getTodo(const std::string& id);
    std::unordered_map<std::string, std::shared_ptr<LocalTodo>> mMap;
    std::unordered_map<std::string, grpc::ServerWriter<todo::v1::Todo>* > mTopicRequesterMap[kMaxTopics];
};


