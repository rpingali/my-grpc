/*
 *
 * Copyright 2015 gRPC authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <iostream>
#include <memory>
#include <string>

#include <grpcpp/grpcpp.h>

#include "api/todo/v1/todo.grpc.pb.h"

#include <future>


using grpc::Status;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;

using todo::v1::TodoService;
using todo::v1::GetTodoRequest;
using todo::v1::CreateTodoRequest;
using todo::v1::CreateTodoResponse;
using todo::v1::GetTodoResponse;


#include <todo_service.h>
TodoContainer container;


class TodoServiceImpl final : public TodoService::Service {
  todo::v1::Todo MakeTodo(const std::shared_ptr<LocalTodo> ltodo)
  {
      todo::v1::Todo todo;
      todo.set_id(ltodo->mId);
      todo.set_completed(ltodo->mCompleted);
      return todo;
  }
  static void notifySubscriber(Topics type, std::unique_ptr<std::string> id)
  {
    auto todo = container.getTodo(*id);
    std::cout << todo->mId << std::endl;
    std::cout << todo->mCompleted << std::endl;
     for(auto &it : container.mTopicRequesterMap[type])
     {
        std::cout << it.first << std::endl;
     }
  }

  Status GetTodo(ServerContext* context, const GetTodoRequest* request,
                  GetTodoResponse* reply) override {
    
    reply->mutable_item()->CopyFrom(MakeTodo(container.getTodo(request->id())));
    return Status::OK;
  }

  Status CreateTodo(ServerContext* context, const CreateTodoRequest* request,
                  CreateTodoResponse* reply) override {
    
    container.createTodo({request->item().id(), request->item().completed()});
    
    reply->set_id(request->item().id());
    //my_str_ptr = std::unique_ptr<std::string>{new std::string(another_str_var)};
     std::unique_ptr<std::string> ptr = std::make_unique<std::string>(request->item().id());
    std::async(std::launch::async, notifySubscriber, CREATED, std::move(ptr));

    return Status::OK;
  }

  ::grpc::Status Subscribe(::grpc::ServerContext* context, const ::todo::v1::SubscribeRequest* request, ::grpc::ServerWriter< ::todo::v1::Todo>* writer) {
  (void) context;
  (void) request;
  (void) writer;
  std::cout << "new subscriber" << std::endl;
  container.mTopicRequesterMap[request->type()].emplace(request->clientname(), writer);
  //return ::grpc::Status(::grpc::StatusCode::OK, "");
}

};

void RunServer() {
  // std::string server_address("0.0.0.0:50051");
  std::string server_address("unix:/tmp/example.sock");

  TodoServiceImpl service;
  
  ServerBuilder builder;
  // Listen on the given address without any authentication mechanism.
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  // Register "service" as the instance through which we'll communicate with
  // clients. In this case it corresponds to an *synchronous* service.
  builder.RegisterService(&service);
  // Finally assemble the server.
  std::unique_ptr<Server> server(builder.BuildAndStart());
  std::cout << "Server listening on " << server_address << std::endl;

  // Wait for the server to shutdown. Note that some other thread must be
  // responsible for shutting down the server for this call to ever return.
  server->Wait();
}

int main(int argc, char** argv) {
 
  RunServer();

  return 0;
}
