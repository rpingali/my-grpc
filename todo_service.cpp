#include <iostream>
#include <string>
#include <unordered_map>
#include <memory>

#include <todo_service.h>



void TodoContainer::createTodo(LocalTodo todo) 
{
    this->mMap.emplace(std::make_pair(todo.mId, 
            std::make_shared<LocalTodo>(LocalTodo(todo))));
}

std::shared_ptr<LocalTodo> TodoContainer::getTodo(const std::string &id) 
{
    auto it = this->mMap.find(id);
    if(it != this->mMap.end())
    {
        return it->second;
    }
    else 
    {
        std::cout << "NULL" << std::endl;
        return nullptr;
    }
}

