/*
 *
 * Copyright 2015 gRPC authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <iostream>
#include <memory>
#include <string>
#include <unistd.h>
#include <future>

#include <grpcpp/grpcpp.h>

#include "api/todo/v1/todo.grpc.pb.h"

using grpc::Status;
using grpc::Channel;
using grpc::ClientContext;

using todo::v1::TodoService;
using todo::v1::GetTodoRequest;
using todo::v1::CreateTodoRequest;
using todo::v1::CreateTodoResponse;
using todo::v1::GetTodoResponse;

#include <todo_service.h>



class  TodoClient {
 public:

  todo::v1::Todo MakeTodo(const LocalTodo &ltodo)
  {
     todo::v1::Todo todo;
      todo.set_id(ltodo.mId);
      todo.set_completed(ltodo.mCompleted);
      return todo;
  }

 void subscribe(todo::v1::TopicType type)
  {
    ClientContext context;
    ::todo::v1::SubscribeRequest subscribe;
    subscribe.set_type(type);
    subscribe.set_clientname("rohan" + std::to_string(getpid()));
    ::todo::v1::Todo todo;
     std::unique_ptr< ::grpc::ClientReader< ::todo::v1::Todo>> reader = stub_->Subscribe(&context, subscribe);

     while (reader->Read(&todo)) {
       std::cout << todo.completed() << std::endl;
       std::cout << todo.id() << std::endl;
    }
  }

   std::thread subscribeThread(todo::v1::TopicType type) {
          return std::thread([=] { subscribe(type); });
      }

  TodoClient(std::shared_ptr<Channel> channel)
      : stub_(TodoService::NewStub(channel)) {}

  // Assembles the client's payload, sends it and presents the response back
  // from the server.
  std::string CreateTodo(const LocalTodo& ltodo) {
    // Data we are sending to the server.
    CreateTodoRequest request;
   
    request.mutable_item()->CopyFrom(MakeTodo(ltodo));
    
    
    // Container for the data we expect from the server.
    CreateTodoResponse reply;

    // Context for the client. It could be used to convey extra information to
    // the server and/or tweak certain RPC behaviors.
    ClientContext context;

    // The actual RPC.
    Status status = stub_->CreateTodo(&context, request, &reply);

    // Act upon its status.
    if (status.ok()) {
      //return reply.id();
    } else {
      std::cout << status.error_code() << ": " << status.error_message()
                << std::endl;
      return "RPC failed";
    }


     GetTodoRequest get_request;
   
     get_request.set_id(ltodo.mId);
    
    ClientContext context1;
    // Container for the data we expect from the server.
    GetTodoResponse get_reply;

     status = stub_->GetTodo(&context1, get_request, &get_reply);
     if (status.ok()) {
       std::cout << get_reply.item().completed() << std::endl;
       std::cout << get_reply.item().id() << std::endl;
    } else {
      std::cout << status.error_code() << ": " << status.error_message()
                << std::endl;
      return "RPC failed";
    }
    return "Done";
  }

 private:
  std::unique_ptr<TodoService::Stub> stub_;
};

int main(int argc, char** argv) {
  // Instantiate the client. It requires a channel, out of which the actual RPCs
  // are created. This channel models a connection to an endpoint (in this case,
  // localhost at port 50051). We indicate that the channel isn't authenticated
  // (use of InsecureChannelCredentials()).
  // TodoClient todoClient(grpc::CreateChannel(
  //     "0.0.0.0:50051", grpc::InsecureChannelCredentials()));

  
  TodoClient todoClient(grpc::CreateChannel(
      "unix:/tmp/example.sock", grpc::InsecureChannelCredentials()));
  std::string user("world");

  std::thread t1 = todoClient.subscribeThread(todo::v1::TopicType::CREATED);
  
  LocalTodo l = LocalTodo({"TODO "+std::to_string(getpid()), false});
  std::string reply = todoClient.CreateTodo(std::ref(l));
  t1.join();

  return 0;
}
